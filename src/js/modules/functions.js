//Перевірка підтримує webp, додавання класса webp аба no-webp для HTML
export function isWebp() {
  function testWebP(callback) {
    const webP = new Image();
    webP.onload = webP.onerror = function () {
      callback(webP.height === 2);
    };
    webP.src =
      "data:image/webp;base64,UklGRjoAAABXRUJQVlA4IC4AAACyAgCdASoCAAIALmk0mk0iIiIiIgBoSygABc6WWgAA/veff/0PP8bA//LwYAAA";
  }

  testWebP(function (support) {
    const body = document.querySelector("body");
    body.classList.remove("webp", "no-webp"); // Видалення інших класів, якщо вони є
    body.classList.add(support ? "webp" : "no-webp");
  });
}
